
---
title: "Lions MD410 2022 Convention Program"
draft: false
---

The Lions MD410 2022 Convention will be held on Friday 29 April and Saturday 30 April 2021. Some Lions serving in District and Multiple District portfolios will be attending events on Wednesday 27 and Thursday 28 April but most Lions will only need to arrive on the afternoon of Thursday 28 or during the day on Friday 29 April.

## Monday 26 April 2022

Time | Event
---|---
Whole day | RLLI

## Tuesday 26 April 2022

Time | Event
---|--
Whole day | RLLI

## Wednesday 27 April 2022

Time | Event
---|---
Whole day | RLLI
Evening | Outgoing and Incoming Council dinners

## Thursday 28 April 2022

Time | Event
---|---
Morning | RLLI
12:00- | MD410 3rd Council meeting (including light lunch)
Evening | PDGs dinner (inclusive of registration cost)
Evening | Welcome evening (inclusive of registration cost)

## Friday 29 April 2022

Time | Event
---|---
Morning | District 410E Cabinet meeting
Morning | District 410W Cabinet meeting
Afternoon  | District 410W Convention
Evening | Banquet

## Saturday 30 April 2022

Time | Event
---|---
Morning | Presidents Elect Breakfast (No additional charge)
Morning | Multiple District 410 Convention 
Morning | Partner's Program
Lunch   | Light lunch
Lunch   | Melvin Jones lunch (no additional charge)
Evening | Theme Evening (Razzle Dazzle/"Night of the Stars")
