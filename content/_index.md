---
title: "Lions MD410 2023 Convention"
date: 2019-07-14T16:19:07+02:00
draft: false
---

Welcome to the 2023 [Lions MD410](https://www.lionsclubs.co.za) Convention website, to be held in late April 2023 in Durban.

This site will be updated after the District 410E and 410W Midyear conferences with registration details, venue details and convention news.
